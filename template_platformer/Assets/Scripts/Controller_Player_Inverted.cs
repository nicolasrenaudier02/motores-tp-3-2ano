﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Inverted : Controller_Player
{
    bool isMovingDown = false;

    public override void FixedUpdate()
    {
        if (isMovingDown)
        {
            base.rb.AddForce(new Vector3(0, -30f, 0));
        }
        else
        {
            base.rb.AddForce(new Vector3(0, 30f, 0));
        }
        base.FixedUpdate();
    }

    public override bool IsOnSomething()
    {
        return Physics.BoxCast(transform.position, new Vector3(transform.localScale.x * 0.9f, transform.localScale.y / 3, transform.localScale.z * 0.9f), Vector3.up, out downHit, Quaternion.identity, downDistanceRay);
    }

    public override void Jump()
    {
        if (IsOnSomething())
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (isMovingDown)
                {
                    base.rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
                }
                else
                {
                    base.rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse);
                }
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            isMovingDown = !isMovingDown;
        }
    }
}
