using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int health = 3; // La salud del enemigo
    public float jumpForce = 5f; // La fuerza con la que el enemigo saltar�

    private Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            // Si la salud del enemigo llega a cero o menos, destruir el objeto
            Destroy(gameObject);
        }
    }

    public void Jump()
    {
        // Hacer que el enemigo salte
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}
