using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player_Tamaño2 : MonoBehaviour
{
    public float shrinkRate = 0.1f; // Tasa de decrecimiento
    public float minScale = 0.5f; // Tamaño mínimo que puede alcanzar el personaje

    private void OnCollisionStay(Collision collision)
    {
        // Verificamos si la colisión es con otro jugador
        if (collision.gameObject.CompareTag("Player"))
        {
            // Disminuimos la escala del jugador mientras se mantengan en contacto
            collision.gameObject.transform.localScale -= Vector3.one * shrinkRate * Time.deltaTime;
            // Limitamos la escala mínima
            if (collision.gameObject.transform.localScale.x < minScale)
            {
                collision.gameObject.transform.localScale = Vector3.one * minScale;
            }
        }
    }
}
