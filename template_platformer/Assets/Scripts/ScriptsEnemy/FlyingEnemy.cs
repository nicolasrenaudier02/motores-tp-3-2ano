using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour
{
    public float speed = 5f; // Velocidad del enemigo
    public float verticalRange = 3f; // Rango vertical del movimiento
    public float verticalSpeed = 2f; // Velocidad vertical del movimiento

    private Vector3 startingPosition;

    void Start()
    {
        startingPosition = transform.position;
    }

    void Update()
    {
        // Movimiento horizontal
        transform.position += transform.right * speed * Time.deltaTime;

        // Movimiento vertical
        float verticalOffset = Mathf.Sin(Time.time * verticalSpeed) * verticalRange;
        transform.position = new Vector3(transform.position.x, startingPosition.y + verticalOffset, transform.position.z);
    }
}