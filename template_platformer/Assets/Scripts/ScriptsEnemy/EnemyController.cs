using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 5f; // Velocidad del enemigo
    public float stoppingDistance = 1f; // Distancia a la que el enemigo se detiene del jugador
    public Transform player; // Transform del jugador

    private void Update()
    {
        // Si el jugador est� fuera de la distancia de detenci�n, perseguirlo
        if (Vector3.Distance(transform.position, player.position) > stoppingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
        }
    }
}