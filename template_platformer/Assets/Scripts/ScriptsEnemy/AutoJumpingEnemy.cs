using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoJumpingEnemy : MonoBehaviour
{
    public float jumpForce = 5f; // Fuerza del salto
    public float jumpInterval = 2f; // Intervalo de tiempo entre saltos

    private Rigidbody rb;
    private float timeSinceLastJump;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        timeSinceLastJump = jumpInterval;
    }

    void Update()
    {
        timeSinceLastJump += Time.deltaTime;

        if (timeSinceLastJump > jumpInterval)
        {
            timeSinceLastJump = 0f;
            Jump();
        }
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }
}